using System;
using System.Collections;
using System.Collections.Generic;
using Events;
using GameLogic.Core;
using SimpleFramework;
using UnityEngine;

public class Env2Window : MonoBehaviour
{
    private void OnTriggerEnter(Collider other)
    {
        MgCore.Get<EventSystem>().Trigger(new YahuanCloneAndWindow()
        {
            isInside = true
        });
    }
    
    private void OnTriggerExit(Collider other)
    {
        MgCore.Get<EventSystem>().Trigger(new YahuanCloneAndWindow()
        {
            isInside = false
        });
    }
}
