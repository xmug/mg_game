using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SimpleFramework
{
    public interface IBindableProperty
    {
        
    }
    
    public class BindableProperty<T> : IBindableProperty where T : IEquatable<T>
    {
        private T _value;

        public BindableProperty(T defaultValue = default(T))
        {
            _value = defaultValue;
        }

        public T Value
        {
            get => _value;
            set
            {
                if (!value.Equals(_value))
                {
                    _value = value;
                    
                    OnValueChanged?.Invoke(_value);
                }
            }
        }

        public Action<T> OnValueChanged;
    }
}