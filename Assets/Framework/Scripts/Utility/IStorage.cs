namespace SimpleFramework
{
    public interface IStorage : BaseCoreComponent
    {
        void SaveInt(string key, int value);
        int LoadInt(string key, int defaultValue = 0);
    }
}