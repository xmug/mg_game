﻿using System;
using UnityEngine;

namespace SimpleFramework
{
    public interface ICommand<T>
    {
        void Execute(T param);
    }
}