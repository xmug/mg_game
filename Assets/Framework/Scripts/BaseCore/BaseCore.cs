﻿using System;
using System.Collections.Generic;
using UnityEditor.VersionControl;
using UnityEngine;

namespace SimpleFramework
{
    public abstract class BaseCore<T> where T: BaseCore<T>, new()
    {
        private static T _instance;

        private static T Instance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = new T();
                    _instance.Init();
                }
                return _instance;
            }
        }
        
        // BaseCore 初始化
        protected BaseCore()
        {
            _initTable = new Dictionary<object, bool>();
            _iocContainer = new IOCContainer();
        }

        private IOCContainer _iocContainer;
        private Dictionary<object, bool> _initTable;

        // IOC容器注册
        protected abstract void Init();
        
        public static T1 Get<T1>() where T1 : class, BaseCoreComponent
        {
            var rt = Instance._iocContainer.Get<T1>();

            if (rt == null)
            {
                Debug.LogError(typeof(T1) + " is not in the iocContainer");
            }
            else if(!rt.IsInit)
            {
                // 懒初始化
                rt.Init();
                rt.IsInit = true;
            }
            
            return rt;
        }

        protected void Register<T1>(T1 instance)
        {
            Instance._iocContainer.Register<T1>(instance);
        }
    }
}