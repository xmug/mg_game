using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SimpleFramework
{
    /// <summary>
    /// 定义参数的事件体，名字作为事件类型，内容作为参数
    /// </summary>
    public interface IEvent<T> where T : struct
    {
        
    }
}
