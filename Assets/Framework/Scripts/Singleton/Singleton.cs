﻿using System;
using System.Reflection;

namespace SimpleFramework
{
    public class Singleton<T> where T : Singleton<T>
    {
        private static T _instance;

        protected static T Instance
        {
            get
            {
                if (_instance == null)
                {
                    var type = typeof(T);
                    var ctors = type.GetConstructors(BindingFlags.Instance | BindingFlags.NonPublic);
                    var ctor = Array.Find(ctors, c => c.GetParameters().Length == 0);

                    if (ctor == null)
                    {
                        throw new Exception("Non public constructor cannot be found in " + type.Name);
                    }

                    _instance = ctor.Invoke(null) as T;
                }

                return _instance;
            }
        }
    }
}