using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TmpAnimSpeedController : MonoBehaviour
{
    private Animator anim;
    [SerializeField] private float speedScale = 1.0f;

    void Awake()
    {
        anim = GetComponent<Animator>();
    }

    private void Update()
    {
        anim.speed = speedScale;
    }
}
