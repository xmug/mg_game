﻿using System;
using SimpleFramework;

namespace Events
{
    public struct MirrorFocusEvent : IEvent<MirrorFocusEvent>
    {
    }
    public struct GardenStepInEvent : IEvent<GardenStepInEvent>
    {
    }
    
    public struct MirrorUnFocusEvent : IEvent<MirrorUnFocusEvent>
    {
    }
    
    public struct SmoothTrasactionEvent : IEvent<SmoothTrasactionEvent>
    {
    }
    
    // public struct GardenStepInEvent : IEvent<GardenStepInEvent>
    // {
    // }
}