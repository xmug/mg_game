﻿using System;
using SimpleFramework;

namespace Events
{
    public struct AssemblableMechEvent : IEvent<AssemblableMechEvent>
    {
        public Type mechType;
    }
}