using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace UILogic
{
    public class UIManager : MonoBehaviour
    {
        private void Start()
        {
            // GameLogic.Player.PlayerModel.hp.OnValueChanged += OnPlayerHPChanged;
        }

        private void OnPlayerHPChanged(int newHP)
        {
            Debug.Log("HP Changed to: " + newHP);
        }
    }
}

