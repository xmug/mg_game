﻿using GameLogic.Mech;
using GameLogic.Player;
using SimpleFramework;

namespace GameLogic.Core
{
    public class MgCore : BaseCore<MgCore>
    {
        protected override void Init()
        {
            Register(new EventSystem());
            Register(new PlayerModel());
        }
    }
}