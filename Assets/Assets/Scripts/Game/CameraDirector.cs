using System.Collections;
using Events;
using GameLogic.Core;
using SimpleFramework;
using UnityEngine;

public class CameraDirector : AbstractController
{
    private Animator camAnimator;
    private EventSystem eSys;

    public GameObject[] rooms;

    private void Awake()
    {
        camAnimator = GetComponent<Animator>();
        eSys = MgCore.Get<EventSystem>();
        
        eSys.Register<MirrorFocusEvent>(param =>
        {
            camAnimator.Play("MirrorCam");
        });
        
        eSys.Register<SmoothTrasactionEvent>(param =>
        {
            StartCoroutine(C1());
        });
        
        eSys.Register<MirrorUnFocusEvent>(param =>
        {
            camAnimator.Play("EnvYahuan");
        });

    }

    IEnumerator C1()
    {
        camAnimator.Play("MirrorCam 1");
        yield return new WaitForSeconds(0.5f);
        camAnimator.Play("WindowCam");
        yield return new WaitForSeconds(0.01f);
        foreach (var room in rooms)
        {
            room.SetActive(false);
        }
        camAnimator.Play("Env2Yahuan");
    }
}
