﻿using System.Collections;
using UnityEngine;

namespace GameLogic.Mech
{
    public class DoorMech : AbstractAssemblableMech<DoorMech>, Interactable
    {
        private bool canOpen = false;
        private bool canEnter = false;

        public override void Reaction()
        {
            canOpen = true;
        }

        public void OnMouseEnter()
        {
        }

        public void OnMouseDown()
        {
            if (canEnter)
            {
                Debug.Log("The fucking door has been entered");
            }
            if (canOpen)
            {
                var anim = GetComponent<Animator>();
                anim.enabled = true;
                anim.Play("DoorOpen");
                canEnter = true;
            }
        }
    }
}