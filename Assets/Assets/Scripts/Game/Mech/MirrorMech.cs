﻿using System;
using Events;
using GameLogic.Core;
using ParadoxNotion;
using SimpleFramework;
using UnityEngine;

namespace GameLogic.Mech
{
    public class MirrorMech : MonoBehaviour, Interactable
    {
        private bool isFocus = false;
        private bool isAbleSmoothTrasaction;

        private void Awake()
        {
            MgCore.Get<EventSystem>().Register<YahuanCloneAndWindow>(param =>
            {
                isAbleSmoothTrasaction = param.isInside;
            });
        }

        private void OnDestroy()
        {
            MgCore.Get<EventSystem>().UnRegister<YahuanCloneAndWindow>(param =>
            {
                isAbleSmoothTrasaction = param.isInside;
            });
        }

        public void OnMouseEnter()
        {
        }

        public void OnMouseDown()
        {
            if (!isFocus)
            {
                MgCore.Get<EventSystem>().Trigger(new MirrorFocusEvent()
                {
                });
                isFocus = true;
            }
            else
            {
                if (isAbleSmoothTrasaction)
                {
                    MgCore.Get<EventSystem>().Trigger(new SmoothTrasactionEvent()
                    {
                    });
                }
                else
                {
                    MgCore.Get<EventSystem>().Trigger(new MirrorUnFocusEvent()
                    {
                    });
                    isFocus = false;
                }
            }
        }
    }
}