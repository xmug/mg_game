﻿using System;
using Events;
using GameLogic.Core;
using SimpleFramework;

namespace GameLogic.Mech
{
    public interface IAssemblableMech
    {
        Type thisType { set; get; }
    }

    public abstract class AbstractAssemblableMech<T> : AbstractController, IAssemblableMech where T : AbstractAssemblableMech<T>
    {
        private void Awake()
        {
            MgCore.Get<EventSystem>().Register<AssemblableMechEvent>(param =>
            {
                if (typeof(T) == param.mechType)
                {
                    Reaction();
                }
            });
        }

        public abstract void Reaction();

        public Type thisType
        {
            get
            {
                return typeof(T);
            }
            set{}
        }
    }
}