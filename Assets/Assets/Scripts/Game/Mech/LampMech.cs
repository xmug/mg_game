﻿using System;
using Events;
using GameLogic.Core;
using SimpleFramework;
using UnityEngine;

namespace GameLogic.Mech
{
    public class LampMech : AbstractController
    {
        private void OnTriggerEnter(Collider other)
        {
            // TODO : detect firefly
            if (other.CompareTag("FireFly"))
            {
                // TODO : make the lamp light
                Debug.Log("Lamp is open");
            }
        }
    }
}