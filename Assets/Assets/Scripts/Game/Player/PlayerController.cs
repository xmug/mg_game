using System;
using System.Collections;
using System.Collections.Generic;
using System.Numerics;
using GameLogic.Core;
using SimpleFramework;
using UnityEngine;
using Quaternion = UnityEngine.Quaternion;
using Vector2 = UnityEngine.Vector2;
using Vector3 = UnityEngine.Vector3;

namespace GameLogic.Player
{
    [RequireComponent(typeof(Rigidbody))]
    public class PlayerController : AbstractController
    {
        
        
        private Rigidbody rb;
        public PlayerModel model;
        private Vector3 curDir = new Vector3(1, 0, 0);


        private void Start()
        {
            rb = this.GetComponentInChildren<Rigidbody>();
            model = MgCore.Get<PlayerModel>();
        }

        private void Update()
        {
            // No need vertical movement
            // Vector2 input = new Vector2(Input.GetAxisRaw("Horizontal"), Input.GetAxisRaw("Vertical"));

            // Input
            Vector2 input = new Vector2(Input.GetAxisRaw("Horizontal"), 0);
            input.Normalize();

            // Turning
            Vector3 dir = new Vector3(input.x, 0, 0);
            Quaternion quaDir = Quaternion.LookRotation(input.x != 0 ? curDir = dir : curDir, Vector3.up);
            transform.rotation = Quaternion.Lerp(transform.rotation, quaDir, Time.deltaTime * model.turningSpeed);

            // Move
            rb.velocity = new Vector3(model.maxForwardSpeed * input.x,
                0, model.maxForwardSpeed * input.y);
            
            // Climb
            if (Physics.Raycast(this.transform.position, this.transform.forward, out RaycastHit hitInfo,
                model.handDistance))
            {
                if (hitInfo.transform.CompareTag("Ladder"))
                {
                    Debug.DrawLine(this.transform.position, this.transform.forward * model.handDistance, Color.red,
                        1.0f);
                    rb.velocity = new Vector3(rb.velocity.x, input.y * model.climbingSpeed, 0);
                }
            }
        }
    }
}