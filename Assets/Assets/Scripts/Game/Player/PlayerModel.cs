using System;
using SimpleFramework;

namespace GameLogic.Player
{
    [Serializable]
    public class PlayerModel : IModel
    {
        public BindableProperty<int> hp = new BindableProperty<int>()
        {
            Value = 0
        };
        public BindableProperty<int> speed = new BindableProperty<int>()
        {
            Value = 0
        };
        
        public float handDistance = 1;
        public float climbingSpeed = 1;
        public float maxForwardSpeed = 1; // How fast can our char move
        public float turningSpeed = 20;
        
        public bool IsInit { get; set; }
        public void Init()
        {
            
        }
    }
}