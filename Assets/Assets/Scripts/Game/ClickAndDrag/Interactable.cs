﻿using System;
using SimpleFramework;

namespace GameLogic.Mech
{
    public interface Interactable : IAttribute
    {
        void OnMouseEnter();
        void OnMouseDown();
    }
}