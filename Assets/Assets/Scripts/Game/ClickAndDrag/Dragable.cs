﻿using System;
using Events;
using GameLogic.Core;
using SimpleFramework;
using UnityEngine;

namespace GameLogic.Mech
{
    public class Dragable : AbstractController, IAttribute
    {
        private Camera _currentCamera;
        private Transform _target;
        private bool isLocked = false;

        private void Awake()
        {
            _currentCamera = Camera.main;
            _target = transform;
        }

        private void OnMouseDrag()
        {
            if (isLocked)
                return;

            //得到摄像机到物体的向量
            Vector3 CO_Direction = _target.position - _currentCamera.transform.position;
            //得到摄像机与物体所在平面的距离
            float cPlane = Vector3.Dot(CO_Direction, _currentCamera.transform.forward);
            
            _target.position =
                _currentCamera.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y, cPlane));

            // 贴附物体，并全局call
            var localPosition = _target.localPosition;
            Vector3 posIgnoreZ = new Vector3(localPosition.x, localPosition.y, 0);
            
            if ((posIgnoreZ - Vector3.zero).magnitude < 0.1f)
            {
                isLocked = true;
                _target.localPosition = Vector3.zero;
                _target.localRotation = Quaternion.identity;
                var assemblableScript = GetComponentInParent<IAssemblableMech>();
                if (assemblableScript != null)
                {
                    MgCore.Get<EventSystem>().Trigger(new AssemblableMechEvent()
                    {
                        mechType = assemblableScript.thisType
                    });
                }
            }
        }
    }
}